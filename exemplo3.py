# Atualizando elementos #
# Para atualizar o valor de um item no dicionário, pode-se modificar diretamente o valor presente em uma chave #
python = {'linguagem': 'python', 'idade': 29}
python['idade'] = 31

# Pode-se utilizar o método update() #
python = {'linguagem': 'python', 'idade': 29}
python.update({'idade': 29, 'criador': 'Guido van Rossum'})