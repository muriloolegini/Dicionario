# Acessando elementos #
pessoa = {'nome': 'fulano', 'altura': 1.79, 'idade': 31}
print(pessoa['nome'])

# O método get() fornecerá o mesmo resultado. Pode também definir um padrão, caso a chave não seja encontrada #
pessoa = {'nome': 'fulano', 'altura': 1.79, 'idade': 31}
print(pessoa.get('peso', 'Chave não encontrada'))