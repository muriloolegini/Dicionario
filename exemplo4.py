# Deletando elementos #
# Pode-se deletar itens de dicionário utilizando a palavra reservada del #
pessoa = {'nome': 'fulano', 'altura': 1.79, 'idade': 31}
del pessoa['altura']

# Também pode-se utilizar o método pop() #
pessoa = {'nome': 'fulano', 'altura': 1.79, 'idade': 31}
altura = pessoa.pop('altura')