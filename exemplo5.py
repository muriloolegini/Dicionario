# Juntando dicionários #
# Para juntar dois ou mais dicionários, utiliza-se o operador ** #
regulagem = {'max': 10, 'meio': 5, 'min': 0}
extra = {'passo': 2}

# Juntando dicionário com ** #
juncao = {**regulagem, **extra}
print(juncao)