# Criando um dicionário #
dicionario = {'primeiro': 1, 'segundo': 2, 'terceiro': 3}

# Usando a função dict passando as chaves e os valores como parâmetro #
dicionario = dict(primeiro=1, segundo=2, terceiro=3)